# Services in Docker Composer file:

## Apache
    rootDIR: ./www-data
    port: 3080
## PHP 7
    PHP 7.2.2
## Mysql
    Mysql 5.7
    port: 3006
    user: devuser
    pass: devpass
## Phpmyadmin
    port: 3081
    